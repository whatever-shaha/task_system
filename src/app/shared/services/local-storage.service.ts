import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class LocalStorageService {
    setItem(key: string, value: unknown): void {
        localStorage.setItem(key, JSON.stringify(value));
    }

    getItem<T = unknown>(key: string): T | null {
        const item = localStorage.getItem(key);
        if (item) {
            return JSON.parse(item);
        }
        return null;
    }
    removeItem(key: string): void {
        localStorage.removeItem(key);
    }
    clear(): void {
        localStorage.clear();
    }
    length(): number {
        return localStorage.length;
    }
}
