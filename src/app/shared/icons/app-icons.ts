import { homeIcon } from "./home.icon";
import { calendarIcon } from "./calendar.icon";
import { checkboxCheckedIcon } from "./checkbox-checked.icon";
import { usersIcon } from "./users.icon";
import { lineChartIcon } from "./line-chart.icon";
import { userIcon } from "./user.icon";
import { gearIcon } from "./gear.icon";
import { logoutIcon } from "./logout.icon";

export const appIcons: Record<string, string> = {
    home: homeIcon,
    calendar: calendarIcon,
    ["checkbox-checked"]: checkboxCheckedIcon,
    ["line-chart"]: lineChartIcon,
    user: userIcon,
    users: usersIcon,
    gear: gearIcon,
    logout: logoutIcon,
};
