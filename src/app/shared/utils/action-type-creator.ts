export const generateActionTypeCreator = (featureName: string) => {
    return (actionType: string) => `[${featureName}] ${actionType}`;
};
