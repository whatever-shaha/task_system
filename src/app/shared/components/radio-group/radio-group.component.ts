import { Component, Input } from "@angular/core";
import { RadioOption } from "./radio-option";
import { FormControl } from "@angular/forms";

@Component({
    selector: "app-radio-group",
    templateUrl: "./radio-group.component.html",
    styleUrl: "./radio-group.component.scss",
})
export class RadioGroupComponent {
    @Input({ required: true }) options: RadioOption[] = [];
    @Input({ required: true }) name!: string;
    @Input({ required: true }) control!: FormControl;
}
