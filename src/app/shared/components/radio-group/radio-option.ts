export interface RadioOption {
    value: string | number;
    label: string;
    color: string;
}
