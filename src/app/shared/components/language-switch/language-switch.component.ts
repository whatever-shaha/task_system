import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { LANG_LOCAL_STORAGE_KEY, RU_LANG, UZ_LANG } from "../../../core/constants";

@Component({
    selector: "app-language-switch",
    templateUrl: "./language-switch.component.html",
    styleUrls: ["./language-switch.component.scss"],
})
export class LanguageSwitchComponent {
    constructor(protected readonly translate: TranslateService) {}

    onSwitch() {
        const nextLang = this.translate.defaultLang === RU_LANG ? UZ_LANG : RU_LANG;
        this.translate.setDefaultLang(nextLang);
        localStorage.setItem(LANG_LOCAL_STORAGE_KEY, nextLang);
    }
}
