import { Component, ElementRef, Input } from "@angular/core";
import { appIcons } from "../../icons/app-icons";

@Component({
    selector: "app-icon",
    templateUrl: "./icon.component.html",
    styleUrl: "./icon.component.scss",
})
export class IconComponent {
    @Input({ required: true })
    set name(iconName: string) {
        this.element.nativeElement.innerHTML = appIcons[iconName];
    }
    constructor(private readonly element: ElementRef) {}
}
