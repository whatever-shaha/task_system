import { Component, Input } from "@angular/core";
import { AbstractControl } from "@angular/forms";

@Component({
    selector: "app-validation-errors-renderer",
    templateUrl: "./validation-errors-renderer.component.html",
    styleUrls: ["./validation-errors-renderer.component.scss"],
})
export class ValidationErrorsRendererComponent {
    @Input() control!: AbstractControl;

    getErrorType() {
        return Object.keys(this.control.errors || {})[0];
    }
}
