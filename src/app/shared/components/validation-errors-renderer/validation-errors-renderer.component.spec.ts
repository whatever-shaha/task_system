import { ComponentFixture, TestBed } from "@angular/core/testing";

import { ValidationErrorsRendererComponent } from "./validation-errors-renderer.component";

describe("ValidationErrorsRendererComponent", () => {
    let component: ValidationErrorsRendererComponent;
    let fixture: ComponentFixture<ValidationErrorsRendererComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ValidationErrorsRendererComponent],
        }).compileComponents();

        fixture = TestBed.createComponent(ValidationErrorsRendererComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
