import { forwardRef, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IconsProviderModule } from "../icons-provider.module";
import { NzLayoutModule } from "ng-zorro-antd/layout";
import { NzMenuModule } from "ng-zorro-antd/menu";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzGridModule } from "ng-zorro-antd/grid";
import { NzModalModule } from "ng-zorro-antd/modal";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzTypographyModule } from "ng-zorro-antd/typography";
import { TranslateModule } from "@ngx-translate/core";
import { HttpClientModule } from "@angular/common/http";
import { ValidationErrorsRendererComponent } from "./components/validation-errors-renderer/validation-errors-renderer.component";
import { LanguageSwitchComponent } from "./components/language-switch/language-switch.component";
import { NzTagModule } from "ng-zorro-antd/tag";
import { IconComponent } from "./components/icon/icon.component";
import { NzDatePickerModule } from "ng-zorro-antd/date-picker";
import { NzRadioModule } from "ng-zorro-antd/radio";
import { NzSpaceModule } from "ng-zorro-antd/space";
import { NzFlexModule } from "ng-zorro-antd/flex";
import { FormsModule, NG_VALUE_ACCESSOR, ReactiveFormsModule } from "@angular/forms";
import { NzSelectModule } from "ng-zorro-antd/select";
import { NzFormModule } from "ng-zorro-antd/form";
import { RadioGroupComponent } from "./components/radio-group/radio-group.component";

const reExportedModules = [
    CommonModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzGridModule,
    NzModalModule,
    NzInputModule,
    NzTypographyModule,
    NzTagModule,
    TranslateModule,
    HttpClientModule,
    NzRadioModule,
    NzSpaceModule,
    NzFlexModule,
    NzDatePickerModule,
    FormsModule,
    NzSelectModule,
    NzFormModule,
];
const reExportedComponents = [
    ValidationErrorsRendererComponent,
    LanguageSwitchComponent,
    IconComponent,
    RadioGroupComponent,
];

@NgModule({
    declarations: [reExportedComponents],
    imports: [reExportedModules, ReactiveFormsModule],
    exports: [reExportedModules, reExportedComponents],
})
export class SharedModule {}
