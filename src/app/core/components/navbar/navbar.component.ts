import { Component } from "@angular/core";
import { NavLink } from "../../models/nav-link";
import { AppRoutes } from "../../constants/app-routes";
import { Store } from "@ngrx/store";
import { AuthActions } from "../../../features/authorization/store/auth.actions";

@Component({
    selector: "app-navbar",
    templateUrl: "./navbar.component.html",
    styleUrl: "./navbar.component.scss",
})
export class NavbarComponent {
    readonly links: NavLink[] = [
        { link: AppRoutes.Main, icon: "home", label: "navbar.links.home" },
        { link: AppRoutes.Task, icon: "checkbox-checked", label: "navbar.links.tasks" },
        { link: AppRoutes.Calendar, icon: "calendar", label: "navbar.links.calendar" },
        { link: AppRoutes.Team, icon: "users", label: "navbar.links.team" },
        { link: AppRoutes.Stats, icon: "line-chart", label: "navbar.links.stats" },
        { link: AppRoutes.Profile, icon: "user", label: "navbar.links.profile" },
    ];

    readonly settingsLink = { link: AppRoutes.Settings, icon: "gear", label: "navbar.links.settings" };

    constructor(private readonly store$: Store) {}

    handleLogout() {
        this.store$.dispatch(AuthActions.logout());
    }
}
