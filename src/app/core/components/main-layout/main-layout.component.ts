import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { selectIsAuthModule } from "../../router-store/router.selectors";

@Component({
    selector: "app-main-layout",
    templateUrl: "./main-layout.component.html",
    styleUrls: ["./main-layout.component.scss"],
})
export class MainLayoutComponent {
    isAuthModule$ = this.store$.select(selectIsAuthModule);

    constructor(private readonly store$: Store) {}
}
