import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { selectUser } from "../../../features/authorization/store/auth.selectors";

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrl: "./header.component.scss",
})
export class HeaderComponent {
    user$ = this.store$.select(selectUser);

    constructor(private readonly store$: Store) {}
}
