import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { selectUrl } from "../../router-store/router.selectors";
import { AppRoutes } from "../../constants/app-routes";
import { map } from "rxjs/operators";
import { TasksHeaderPluginComponent } from "../header-plugin-components/tasks-header-plugin/tasks-header-plugin.component";
import { CalendarHeaderPluginComponent } from "../header-plugin-components/calendar-header-plugin/calendar-header-plugin.component";

@Component({
    selector: "app-header-plugins",
    templateUrl: "./header-plugins.component.html",
    styleUrl: "./header-plugins.component.scss",
})
export class HeaderPluginsComponent {
    currentUrl$ = this.store$.select(selectUrl);

    currentComponent$ = this.currentUrl$.pipe(
        map((currentUrl) => {
            return this.pluginComponents.find(({ path }) => currentUrl.includes(path))?.component;
        }),
    );

    readonly pluginComponents = [
        {
            path: AppRoutes.Task,
            component: TasksHeaderPluginComponent,
        },
        { path: AppRoutes.Calendar, component: CalendarHeaderPluginComponent },
    ];

    constructor(private readonly store$: Store) {}
}
