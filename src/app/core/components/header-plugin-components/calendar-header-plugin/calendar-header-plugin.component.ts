import { Component } from "@angular/core";

@Component({
    selector: "app-calendar-header-plugin",
    templateUrl: "./calendar-header-plugin.component.html",
    styleUrl: "./calendar-header-plugin.component.scss",
})
export class CalendarHeaderPluginComponent {}
