import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { SelectOption } from "../../../models/select-option";
import { SortOption } from "../../../constants/sort-option";
import { selectTaskSortOption } from "../../../../features/task/store/task.selectors";
import { TaskActions } from "../../../../features/task/store/task.actions";

@Component({
    selector: "app-tasks-header-plugin",
    templateUrl: "./tasks-header-plugin.component.html",
    styleUrl: "./tasks-header-plugin.component.scss",
})
export class TasksHeaderPluginComponent {
    sortOption$ = this.store$.select(selectTaskSortOption);

    readonly selectOptions: SelectOption[] = [
        { label: "common.sort-labels.date-asc", value: SortOption.DATE_ASC },
        { label: "common.sort-labels.date-desc", value: SortOption.DATE_DESC },
    ];

    constructor(private readonly store$: Store) {}

    handleTaskCreateClick() {
        this.store$.dispatch(TaskActions.openTaskModal());
    }

    handleSortValueChange($event: SortOption) {
        this.store$.dispatch(TaskActions.setTaskSortOption({ data: $event }));
    }
}
