import { AbstractControl } from "@angular/forms";

export function updateValidity(controls: Record<string, AbstractControl>): void {
    Object.values(controls)
        .filter(({ invalid }) => invalid)
        .forEach((control) => {
            control.markAsDirty();
            control.updateValueAndValidity({ onlySelf: true });
        });
}
