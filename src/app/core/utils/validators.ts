import { AbstractControl, ValidationErrors } from "@angular/forms";
import { PASSWORD_FIELD_NAME, PHONE_NUMBER_REGEXP } from "../constants";

export function phoneNumberValidator(control: AbstractControl): ValidationErrors | null {
    if (PHONE_NUMBER_REGEXP.test(control.value)) return null;
    return { phone: true };
}

export function repeatPasswordValidator(control: AbstractControl): ValidationErrors | null {
    if (control.parent?.get(PASSWORD_FIELD_NAME)?.value === control.value) return null;
    return { "repeat-password": true };
}
