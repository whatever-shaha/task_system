import { ResolveFn, Router } from "@angular/router";
import { inject } from "@angular/core";
import { Store } from "@ngrx/store";
import { selectIsAuthenticated, selectUser } from "../../features/authorization/store/auth.selectors";
import { map, tap, withLatestFrom } from "rxjs/operators";
import { AppRoutes } from "../constants/app-routes";

export const loginResolve: ResolveFn<boolean> = () => {
    const store$ = inject(Store);
    const router = inject(Router);
    return store$.select(selectIsAuthenticated).pipe(
        withLatestFrom(store$.select(selectUser)),
        tap(([isAuth, user]) => {
            console.log({ isAuth, user });
            if (isAuth) void router.navigate([AppRoutes.Task]);
        }),
        map(([isAuth]) => isAuth),
    );
};
