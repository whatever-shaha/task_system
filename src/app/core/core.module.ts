import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { MainLayoutComponent } from "./components/main-layout/main-layout.component";
import { RouterLink, RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { coreFeature } from "./store/core.reducer";
import { AuthorizationLayoutComponent } from "./components/authorization-layout/authorization-layout.component";
import { HeaderComponent } from "./components/header/header.component";
import { NgOptimizedImage } from "@angular/common";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { NzDividerComponent } from "ng-zorro-antd/divider";
import { HeaderPluginsComponent } from "./components/header-plugins/header-plugins.component";
import { TasksHeaderPluginComponent } from "./components/header-plugin-components/tasks-header-plugin/tasks-header-plugin.component";
import { CalendarHeaderPluginComponent } from "./components/header-plugin-components/calendar-header-plugin/calendar-header-plugin.component";

@NgModule({
    declarations: [
        MainLayoutComponent,
        AuthorizationLayoutComponent,
        HeaderComponent,
        NavbarComponent,
        HeaderPluginsComponent,
        TasksHeaderPluginComponent,
        CalendarHeaderPluginComponent,
    ],
    imports: [
        SharedModule,
        RouterLink,
        RouterModule,
        StoreModule.forFeature(coreFeature.name, coreFeature.reducer),
        NgOptimizedImage,
        NzDividerComponent,
    ],
    exports: [MainLayoutComponent],
})
export class CoreModule {}
