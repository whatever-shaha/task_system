export interface NavLink {
    link: string;
    label: string;
    icon: string;
}
