import { CanActivateFn, Router } from "@angular/router";
import { inject } from "@angular/core";
import { Store } from "@ngrx/store";
import { selectIsAuthenticated } from "../../features/authorization/store/auth.selectors";
import { tap } from "rxjs/operators";
import { AppRoutes } from "../constants/app-routes";

export const authGuard: CanActivateFn = () => {
    const store$ = inject(Store);
    const router = inject(Router);
    return store$.select(selectIsAuthenticated).pipe(
        tap((isAuth) => {
            if (!isAuth) void router.navigate([AppRoutes.Authorization, AppRoutes.Login]);
        }),
    );
};
