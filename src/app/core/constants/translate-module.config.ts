import { TranslateLoader } from "@ngx-translate/core";
import { HttpClient } from "@angular/common/http";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { LANG_LOCAL_STORAGE_KEY, RU_LANG } from "./index";

export const TRANSLATE_MODULE_CONFIG = {
    loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http),
        deps: [HttpClient],
    },
    defaultLanguage: localStorage.getItem(LANG_LOCAL_STORAGE_KEY) || RU_LANG,
    useDefaultLang: true,
};
