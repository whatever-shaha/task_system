export const enum AppRoutes {
    Index = "",
    Wildcard = "**",
    Authorization = "auth",
    Task = "task",
    Main = "main",
    Calendar = "calendar",
    Settings = "settings",
    Stats = "stats",
    Team = "team",
    Profile = "profile",
    Login = "login",
}
