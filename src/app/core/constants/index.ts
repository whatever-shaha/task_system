export const LANG_LOCAL_STORAGE_KEY = "lang";
export const RU_LANG = "ru";
export const UZ_LANG = "uz";

export const PHONE_NUMBER_PREFIX = "+998";

export const PASSWORD_FIELD_NAME = "password";
export const PHONE_NUMBER_REGEXP = /\d{9}/g;
