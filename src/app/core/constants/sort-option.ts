export const enum SortOption {
    DATE_ASC = "date-asc",
    DATE_DESC = "date-desc",
}
