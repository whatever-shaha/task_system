import { NzConfig } from "ng-zorro-antd/core/config";

export const NG_ZORRO_CONFIG: NzConfig = {
    theme: {
        primaryColor: "red",
    },
};
