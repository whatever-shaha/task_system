import { createFeature, createReducer, on } from "@ngrx/store";
import * as CoreActions from "./core.actions";
import { CORE_FEATURE_KEY } from "../constants/store-feature-keys";

export interface CoreState {
    isNavbarCollapsed: boolean;
}

export const initialState: CoreState = { isNavbarCollapsed: false };

export const reducer = createReducer(
    initialState,
    on(
        CoreActions.setNavbarStateAction,
        (state, { isOpen }): CoreState => ({
            ...state,
            isNavbarCollapsed: isOpen,
        }),
    ),
    on(
        CoreActions.toggleNavbarStateAction,
        (state): CoreState => ({
            ...state,
            isNavbarCollapsed: !state.isNavbarCollapsed,
        }),
    ),
);

export const coreFeature = createFeature({
    name: CORE_FEATURE_KEY,
    reducer,
});
