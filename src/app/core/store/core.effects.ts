import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { tap } from "rxjs/operators";
import * as CoreActions from "./core.actions";

@Injectable()
export class CoreEffects {
    getUsers$ = createEffect(
        () => {
            return this.actions$.pipe(
                ofType(CoreActions.toggleNavbarStateAction),
                tap(
                    (action) => {
                        console.log(action);
                    },
                    // /** An EMPTY observable only emits completion. Replace with your own observable API request */
                    // EMPTY.pipe(
                    //     map(() => CoreActions.setNavbarStateAction({ isOpen: false })),
                    //     catchError(() => of(CoreActions.toggleNavbarStateAction()))
                    // )
                ),
            );
        },
        { dispatch: false },
    );

    constructor(private actions$: Actions) {}
}
