import { createFeatureSelector, createSelector } from "@ngrx/store";
import * as fromCore from "./core.reducer";
import { CORE_FEATURE_KEY } from "../constants/store-feature-keys";

export const selectCoreState = createFeatureSelector<fromCore.CoreState>(CORE_FEATURE_KEY);

export const selectIsNavbarCollapsed = createSelector(selectCoreState, (state) => state.isNavbarCollapsed);
