import { createAction, props } from "@ngrx/store";
import { generateActionTypeCreator } from "../../shared/utils/action-type-creator";
import { CORE_FEATURE_KEY } from "../constants/store-feature-keys";

const actionTypeCreator = generateActionTypeCreator(CORE_FEATURE_KEY);
export const setNavbarStateAction = createAction(
    actionTypeCreator("set navbar state"),
    props<{ isOpen: boolean }>(),
);

export const toggleNavbarStateAction = createAction(actionTypeCreator("toggle navbar state"));
