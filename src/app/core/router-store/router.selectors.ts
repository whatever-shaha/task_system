import { createSelector } from "@ngrx/store";
import { getRouterSelectors } from "@ngrx/router-store";
import { AUTHORIZATION_MODULE_PATH } from "./router-store.constants";

export const { selectUrl } = getRouterSelectors();

export const selectIsPageOpen = (path: string) =>
    createSelector(selectUrl, (url: string) => {
        return url.includes(path);
    });

export const selectIsAuthModule = selectIsPageOpen(AUTHORIZATION_MODULE_PATH);
export const selectRouterStoreInitialized = createSelector(selectUrl, Boolean);
