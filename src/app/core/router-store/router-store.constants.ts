import { AppRoutes } from "../constants/app-routes";

export const AUTHORIZATION_MODULE_PATH = `/${AppRoutes.Authorization}/`;
