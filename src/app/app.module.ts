import { isDevMode, NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { NZ_I18N, ru_RU } from "ng-zorro-antd/i18n";
import { registerLocaleData } from "@angular/common";
import ru from "@angular/common/locales/ru";
import { SharedModule } from "./shared/shared.module";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { CoreModule } from "./core/core.module";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { DEFAULT_ROUTER_FEATURENAME, routerReducer, StoreRouterConnectingModule } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CoreEffects } from "./core/store/core.effects";
import { TranslateModule } from "@ngx-translate/core";
import { TRANSLATE_MODULE_CONFIG } from "./core/constants/translate-module.config";
import { authFeature } from "./features/authorization/store/auth.reducer";
import { AuthEffects } from "./features/authorization/store/auth.effects";

registerLocaleData(ru);

@NgModule({
    declarations: [AppComponent],
    imports: [
        StoreModule.forRoot({
            [DEFAULT_ROUTER_FEATURENAME]: routerReducer,
            [authFeature.name]: authFeature.reducer,
        }),
        SharedModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        CoreModule,
        EffectsModule.forRoot([CoreEffects, AuthEffects]),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() }),
        StoreRouterConnectingModule.forRoot(),
        TranslateModule.forRoot(TRANSLATE_MODULE_CONFIG),
    ],
    providers: [{ provide: NZ_I18N, useValue: ru_RU }],
    bootstrap: [AppComponent],
})
export class AppModule {}
