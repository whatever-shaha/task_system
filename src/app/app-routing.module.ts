import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppRoutes } from "./core/constants/app-routes";
import { authGuard } from "./core/guards/auth.guard";
import { loginResolve } from "./core/resolvers/login.resolve";

const routes: Routes = [
    {
        path: AppRoutes.Task,
        loadChildren: () => import("./features/task/task.module").then((m) => m.TaskModule),
        canActivate: [authGuard],
    },
    {
        path: AppRoutes.Main,
        loadChildren: () => import("./features/main-page/main-page.module").then((m) => m.MainPageModule),
        canActivate: [authGuard],
    },
    {
        path: AppRoutes.Calendar,
        loadChildren: () => import("./features/calendar/calendar.module").then((m) => m.CalendarModule),
        canActivate: [authGuard],
    },
    {
        path: AppRoutes.Authorization,
        loadChildren: () =>
            import("./features/authorization/authorization.module").then((m) => m.AuthorizationModule),
        resolve: [loginResolve],
    },
    { path: AppRoutes.Wildcard, redirectTo: AppRoutes.Task },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { initialNavigation: "enabledBlocking" })],
    exports: [RouterModule],
})
export class AppRoutingModule {}
