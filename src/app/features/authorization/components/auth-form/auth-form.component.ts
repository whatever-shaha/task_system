import { Component, EventEmitter, Output } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { LoginData } from "../../models/login-data";
import { Store } from "@ngrx/store";
import { selectIsLoggingIn } from "../../store/auth.selectors";
import { updateValidity } from "../../../../core/utils/function-utils";

@Component({
    selector: "app-auth-form",
    templateUrl: "./auth-form.component.html",
    styleUrls: ["./auth-form.component.scss"],
})
export class AuthFormComponent {
    @Output() action = new EventEmitter<LoginData>();

    loading$ = this.store$.select(selectIsLoggingIn);

    loginForm = new FormGroup({
        email: new FormControl("", [Validators.required, Validators.email]),
        password: new FormControl("", [Validators.required, Validators.minLength(6)]),
    });

    constructor(private readonly store$: Store) {}

    submitForm() {
        if (this.loginForm.valid) {
            return this.action.emit(this.loginForm.value as LoginData);
        }
        updateValidity(this.loginForm.controls);
    }
}
