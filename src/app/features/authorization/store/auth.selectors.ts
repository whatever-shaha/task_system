import { createFeatureSelector, createSelector } from "@ngrx/store";
import { authFeature, AuthState } from "./auth.reducer";

export const selectAuthState = createFeatureSelector<AuthState>(authFeature.name);
export const selectIsLoggingIn = createSelector(selectAuthState, (state) => state.loading);
export const selectUser = createSelector(selectAuthState, (state) => state.user);
export const selectIsAuthenticated = createSelector(selectAuthState, (state) => Boolean(state.user.token));
