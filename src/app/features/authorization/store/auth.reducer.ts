import { createFeature, createReducer, on } from "@ngrx/store";
import { AuthActions } from "./auth.actions";
import { AUTH_FEATURE_KEY } from "../../../core/constants/store-feature-keys";
import { AuthUser } from "../models/user";
import { LocalStorageKey } from "../../../core/constants/local-storage-key";

export interface AuthState {
    user: AuthUser;
    loading: boolean;
}

const localStorageUser = localStorage.getItem(LocalStorageKey.User);

const initialUserState = {
    avatar: "",
    id: 0,
    displayName: "",
    email: "",
    token: "",
};
export const initialState: AuthState = {
    user: localStorageUser ? JSON.parse(localStorageUser) : initialUserState,
    loading: false,
};

export const reducer = createReducer(
    initialState,
    on(AuthActions.login, (state): AuthState => ({ ...state, loading: true })),
    on(AuthActions.logout, (state): AuthState => ({ ...state, user: initialUserState })),
    on(AuthActions.loginSuccess, (state, { data }): AuthState => ({ ...state, user: data })),
    on(
        AuthActions.loginFailure,
        AuthActions.loginSuccess,
        (state): AuthState => ({ ...state, loading: false }),
    ),
);

export const authFeature = createFeature({
    name: AUTH_FEATURE_KEY,
    reducer,
});
