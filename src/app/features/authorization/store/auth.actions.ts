import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { LoginData } from "../models/login-data";
import { AuthUser } from "../models/user";

export const AuthActions = createActionGroup({
    source: "User",
    events: {
        login: props<LoginData>(),
        logout: emptyProps(),
        "login Success": props<{ data: AuthUser }>(),
        "login Failure": props<{ error: string }>(),
    },
});
