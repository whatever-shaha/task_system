import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { delay, of } from "rxjs";
import { AuthActions } from "./auth.actions";
import { catchError, map, switchMap, tap } from "rxjs/operators";
import { AuthUser } from "../models/user";
import { MOCK_APP_USERS } from "../../../../assets/mocks/users.mock";
import { LocalStorageKey } from "../../../core/constants/local-storage-key";
import { Router } from "@angular/router";
import { AppRoutes } from "../../../core/constants/app-routes";
import { NzMessageService } from "ng-zorro-antd/message";
import { TranslateService } from "@ngx-translate/core";

@Injectable()
export class AuthEffects {
    login$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(AuthActions.login),
            switchMap(({ email, password }) => {
                const user = MOCK_APP_USERS.find((u) => u.password === password && u.email === email);
                if (!user) {
                    return of(AuthActions.loginFailure({ error: "error.incorrect-credentials" }));
                }
                return of({ ...user, token: "mock-token" } as AuthUser).pipe(
                    delay(300),
                    map((data) => AuthActions.loginSuccess({ data })),
                    catchError((error) => of(AuthActions.loginFailure({ error }))),
                );
            }),
        );
    });

    saveUserToLocalStorage$ = createEffect(
        () => {
            return this.actions$.pipe(
                ofType(AuthActions.loginSuccess),
                tap(({ data }) => localStorage.setItem(LocalStorageKey.User, JSON.stringify(data))),
            );
        },
        { dispatch: false },
    );

    navigateAfterLogin$ = createEffect(
        () => {
            return this.actions$.pipe(
                ofType(AuthActions.loginSuccess),
                tap(() => {
                    void this.router.navigate([AppRoutes.Task]);
                }),
            );
        },
        { dispatch: false },
    );

    cleanLocalStorageOnLogout$ = createEffect(
        () => {
            return this.actions$.pipe(
                ofType(AuthActions.logout),
                tap(() => {
                    localStorage.removeItem(LocalStorageKey.User);
                    void this.router.navigate([AppRoutes.Authorization, AppRoutes.Login]);
                }),
            );
        },
        { dispatch: false },
    );

    showLoginError = createEffect(
        () => {
            return this.actions$.pipe(
                ofType(AuthActions.loginFailure),
                tap(({ error }) => {
                    this.messageService.error(this.translate.instant(error));
                    void this.router.navigate([AppRoutes.Authorization, AppRoutes.Login]);
                }),
            );
        },
        { dispatch: false },
    );

    constructor(
        private readonly actions$: Actions,
        private readonly router: Router,
        private readonly messageService: NzMessageService,
        private readonly translate: TranslateService,
    ) {}
}
