export const enum TaskPriority {
    High,
    Medium,
    Low,
}
