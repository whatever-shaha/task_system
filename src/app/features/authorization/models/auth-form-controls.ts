import { AbstractControlOptions } from "@angular/forms";

interface AuthFormControl {
    name: string;
    prefix?: string;
    icon?: "user" | "lock";
    validators: AbstractControlOptions["validators"];
    type: "text" | "password";
}

export type AuthFormControls = AuthFormControl[];
