export interface User {
    id: number;
    displayName: string;
    email: string;
    avatar: string;
}

export interface AuthUser extends User {
    token: string;
}
