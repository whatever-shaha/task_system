import { Task } from "./task";

export interface TaskFormData extends Omit<Task, "user"> {
    userId: number;
}
