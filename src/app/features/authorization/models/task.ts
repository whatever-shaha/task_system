import { TaskPriority } from "../constants/task-priority";
import { User } from "./user";

export interface Task {
    user: User;
    title: string;
    date: string;
    priority: TaskPriority;
    description?: string;
}
