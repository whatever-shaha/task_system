import { Component } from "@angular/core";
import { LoginData } from "../../models/login-data";
import { Store } from "@ngrx/store";
import { AuthActions } from "../../store/auth.actions";

@Component({
    templateUrl: "./login-page.component.html",
    styleUrls: ["./login-page.component.scss"],
})
export class LoginPageComponent {
    constructor(private readonly store$: Store) {}

    onSubmit($event: LoginData) {
        this.store$.dispatch(AuthActions.login($event));
    }
}
