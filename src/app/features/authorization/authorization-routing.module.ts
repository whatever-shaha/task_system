import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppRoutes } from "../../core/constants/app-routes";
import { LoginPageComponent } from "./pages/login/login-page.component";

const routes: Routes = [
    {
        path: AppRoutes.Login,
        component: LoginPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthorizationRoutingModule {}
