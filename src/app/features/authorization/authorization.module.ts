import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";

import { AuthorizationRoutingModule } from "./authorization-routing.module";
import { LoginPageComponent } from "./pages/login/login-page.component";
import { AuthFormComponent } from "./components/auth-form/auth-form.component";
import { SharedModule } from "../../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { NzFormModule } from "ng-zorro-antd/form";

@NgModule({
    declarations: [LoginPageComponent, AuthFormComponent],
    imports: [
        CommonModule,
        AuthorizationRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        NzFormModule,
        NgOptimizedImage,
    ],
})
export class AuthorizationModule {}
