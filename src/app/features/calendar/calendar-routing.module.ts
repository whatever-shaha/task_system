import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppRoutes } from "../../core/constants/app-routes";
import { CalendarPageComponent } from "./pages/calendar-page/calendar-page.component";

const routes: Routes = [{ path: AppRoutes.Index, component: CalendarPageComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CalendarRoutingModule {}
