import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MainPageRoutingModule } from "./main-page-routing.module";
import { MainPageComponent } from "./pages/main-page/main-page.component";

@NgModule({
    declarations: [MainPageComponent],
    imports: [CommonModule, MainPageRoutingModule],
})
export class MainPageModule {}
