import { Component } from "@angular/core";

@Component({
    templateUrl: "./tasks-page.component.html",
    styleUrl: "./tasks-page.component.scss",
})
export class TasksPageComponent {}
