import { Task } from "../../authorization/models/task";
import { SortOption } from "../../../core/constants/sort-option";

export const sortOptionFunctionMap = {
    [SortOption.DATE_ASC]: sortByDate("ASC"),
    [SortOption.DATE_DESC]: sortByDate("DESC"),
};

export function sortByDate(order: "ASC" | "DESC"): SorterFn {
    return (tasks: Task[]) =>
        tasks.sort((a, b) => {
            const aDate = new Date(a.date).getTime();
            const bDate = new Date(b.date).getTime();
            if (order === "ASC") {
                return aDate - bDate;
            }
            return bDate - aDate;
        });
}

type SorterFn = (tasks: Task[]) => Task[];
