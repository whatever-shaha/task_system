export interface PriorityTagConfig {
    bgColor: string;
    color: string;
    label: string;
}
