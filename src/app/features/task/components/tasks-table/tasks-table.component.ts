import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { selectTasks, selectTasksLoading } from "../../store/task.selectors";
import { Observable } from "rxjs";
import { Task } from "../../../authorization/models/task";
import { TaskActions } from "../../store/task.actions";
import { formatDate } from "@angular/common";

@Component({
    selector: "app-tasks-table",
    templateUrl: "./tasks-table.component.html",
    styleUrl: "./tasks-table.component.scss",
})
export class TasksTableComponent implements OnInit {
    tasks$: Observable<Task[]> = this.store$.select(selectTasks);
    tasksLoading$ = this.store$.select(selectTasksLoading);
    currentPage = 1;

    readonly pageSize = 10;

    constructor(private readonly store$: Store) {}

    ngOnInit() {
        this.store$.dispatch(TaskActions.loadTasks());
    }

    formatDate(date: string) {
        return formatDate(date, "longDate", "ru-RU");
    }

    handleRowClick(data: Task) {
        const { user, ...taskData } = data;
        this.store$.dispatch(TaskActions.openTaskModal({ ...taskData, userId: user.id }));
    }

    handleTaskDelete($event: MouseEvent, index: number) {
        $event.stopPropagation();
        this.store$.dispatch(
            TaskActions.deleteTask({ data: index + this.pageSize * (this.currentPage - 1) }),
        );
    }
}
