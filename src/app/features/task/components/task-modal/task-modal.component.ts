import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import {
    selectCurrentTask,
    selectTaskModalOpen,
    selectTaskSaving,
    selectUsers,
    selectUsersLoading,
} from "../../store/task.selectors";
import { TaskActions } from "../../store/task.actions";
import { TaskPriority } from "../../../authorization/constants/task-priority";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { formatDate } from "@angular/common";
import { TaskFormData } from "../../../authorization/models/task-form-data";
import { filter, map } from "rxjs/operators";

@Component({
    selector: "app-task-modal",
    templateUrl: "./task-modal.component.html",
    styleUrl: "./task-modal.component.scss",
})
export class TaskModalComponent implements OnInit {
    isOpen$ = this.store$.select(selectTaskModalOpen);
    usersLoading$ = this.store$.select(selectUsersLoading);
    taskSaving$ = this.store$.select(selectTaskSaving);
    users$ = this.store$.select(selectUsers);
    currentTask$ = this.store$.select(selectCurrentTask);

    form = this.setUpForm();

    readonly priorityOptions = [
        { label: "common.high", value: TaskPriority.High, color: "red" },
        { label: "common.medium", value: TaskPriority.Medium, color: "orange" },
        { label: "common.low", value: TaskPriority.Low, color: "blue" },
    ];

    constructor(private readonly store$: Store) {}

    ngOnInit() {
        this.currentTask$
            .pipe(
                filter(Boolean),
                map((task) => ({ ...task, date: new Date(task.date) })),
            )
            .subscribe((task) => this.form.patchValue(task));
    }

    handleCancel() {
        console.log("close");
        setTimeout(() => this.store$.dispatch(TaskActions.closeTaskModal()));
    }

    handleAfterOpen() {
        this.store$.dispatch(TaskActions.loadUsers());
    }

    handleSubmit() {
        const values = this.form.value;
        this.store$.dispatch(
            TaskActions.saveTask({
                data: {
                    ...values,
                    date: formatDate(values.date!, "YYYY-MM-dd", "ru_RU"),
                } as TaskFormData,
            }),
        );
    }

    handleAfterClose() {
        this.form = this.setUpForm();
    }

    private setUpForm() {
        return new FormGroup({
            date: new FormControl(new Date()),
            title: new FormControl("", [Validators.required]),
            description: new FormControl(""),
            priority: new FormControl<TaskPriority>(TaskPriority.Medium),
            userId: new FormControl<string | number>("", [Validators.required]),
        });
    }
}
