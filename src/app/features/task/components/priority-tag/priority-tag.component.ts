import { Component, Input, OnInit } from "@angular/core";
import { TaskPriority } from "../../../authorization/constants/task-priority";
import { PriorityTagConfig } from "../../models/priority-tag-config";

@Component({
    selector: "app-priority-tag",
    templateUrl: "./priority-tag.component.html",
    styleUrl: "./priority-tag.component.scss",
})
export class PriorityTagComponent implements OnInit {
    @Input({ required: true }) priority!: TaskPriority;

    readonly priorityConfigMap: Record<TaskPriority, PriorityTagConfig> = {
        [TaskPriority.High]: { bgColor: "#FFEEEB", color: "#F2565C", label: "common.high" },
        [TaskPriority.Medium]: { bgColor: "#FAF7E3", color: "#EAA238", label: "common.medium" },
        [TaskPriority.Low]: { bgColor: "#F0F1FF", color: "#4971FF", label: "common.low" },
    };

    config: PriorityTagConfig = this.priorityConfigMap[TaskPriority.Low];

    ngOnInit() {
        this.config = this.priorityConfigMap[this.priority];
    }
}
