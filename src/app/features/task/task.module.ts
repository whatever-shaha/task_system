import { forwardRef, NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";

import { TaskRoutingModule } from "./task-routing.module";
import { TasksPageComponent } from "./pages/tasks-page/tasks-page.component";
import { SharedModule } from "../../shared/shared.module";
import { EffectsModule } from "@ngrx/effects";
import { TaskEffects } from "./store/task.effects";
import { StoreModule } from "@ngrx/store";
import { taskFeature } from "./store/task.reducer";
import { TasksTableComponent } from "./components/tasks-table/tasks-table.component";
import { NzTableModule } from "ng-zorro-antd/table";
import { PriorityTagComponent } from "./components/priority-tag/priority-tag.component";
import { TaskModalComponent } from "./components/task-modal/task-modal.component";
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from "@angular/forms";
import { RadioGroupComponent } from "../../shared/components/radio-group/radio-group.component";

@NgModule({
    declarations: [TasksPageComponent, TasksTableComponent, PriorityTagComponent, TaskModalComponent],
    imports: [
        CommonModule,
        TaskRoutingModule,
        SharedModule,
        StoreModule.forFeature(taskFeature.name, taskFeature.reducer),
        EffectsModule.forFeature([TaskEffects]),
        NzTableModule,
        NgOptimizedImage,
        ReactiveFormsModule,
    ],
    exports: [TasksPageComponent],
})
export class TaskModule {}
