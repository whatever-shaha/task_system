import { createFeatureSelector, createSelector } from "@ngrx/store";
import { TASK_FEATURE_KEY } from "../../../core/constants/store-feature-keys";
import { TaskState } from "./task.reducer";

export const selectTaskState = createFeatureSelector<TaskState>(TASK_FEATURE_KEY);

export const selectTasks = createSelector(selectTaskState, (state) => {
    return state.tasks;
});
export const selectTasksLoading = createSelector(selectTaskState, (state) => state.tasksLoading);
export const selectUsers = createSelector(selectTaskState, (state) => state.users);
export const selectCurrentTask = createSelector(selectTaskState, (state) => state.currentTask);
export const selectUsersLoading = createSelector(selectTaskState, (state) => state.usersLoading);
export const selectTaskSaving = createSelector(selectTaskState, (state) => state.taskSaving);
export const selectTaskSortOption = createSelector(selectTaskState, (state) => state.sortOption);
export const selectTaskModalOpen = createSelector(selectTaskState, (state) => state.taskModalOpen);
