import { createActionGroup, emptyProps, props } from "@ngrx/store";
import { Task } from "../../authorization/models/task";
import { User } from "../../authorization/models/user";
import { SortOption } from "../../../core/constants/sort-option";
import { TaskFormData } from "../../authorization/models/task-form-data";

export const TaskActions = createActionGroup({
    source: "Task",
    events: {
        "Load Tasks": emptyProps(),
        "Load Tasks Success": props<{ data: Task[] }>(),
        "Load Tasks Failure": props<{ error: unknown }>(),
        "Load Users": emptyProps(),
        "Load Users Success": props<{ data: User[] }>(),
        "Load Users Failure": props<{ error: unknown }>(),
        "Set Task Sort Option": props<{ data: SortOption }>(),
        "Open Task Modal": (data?: TaskFormData) => ({ data }),
        "Close Task Modal": emptyProps(),
        "Clean Current Task": emptyProps(),
        "Save Task": props<{ data: TaskFormData }>(),
        "Delete Task": props<{ data: number }>(),
        "Save Task Success": props<{ data: Task }>(),
        "Save Task Failure": props<{ error: unknown }>(),
    },
});
