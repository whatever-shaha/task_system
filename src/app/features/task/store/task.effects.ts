import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, switchMap } from "rxjs/operators";
import { delay, of } from "rxjs";
import { TaskActions } from "./task.actions";
import { MOCK_TASKS } from "../../../../assets/mocks/tasks.mock";
import { MOCK_USERS } from "../../../../assets/mocks/users.mock";
import { User } from "../../authorization/models/user";
import { CLEAN_CURRENT_TASK_DELAY } from "../utils/consts";

@Injectable()
export class TaskEffects {
    loadTasks$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(TaskActions.loadTasks),
            switchMap(() =>
                of(MOCK_TASKS).pipe(
                    delay(300),
                    map((data) => TaskActions.loadTasksSuccess({ data })),
                    catchError((error) => of(TaskActions.loadTasksFailure({ error }))),
                ),
            ),
        );
    });

    loadUsers$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(TaskActions.loadUsers),
            switchMap(() =>
                of(MOCK_USERS).pipe(
                    delay(300),
                    map((data) => TaskActions.loadUsersSuccess({ data })),
                    catchError((error) => of(TaskActions.loadUsersFailure({ error }))),
                ),
            ),
        );
    });

    saveTask$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(TaskActions.saveTask),
            switchMap(({ data }) => {
                const { userId, ...taskData } = data;
                const user = MOCK_USERS.find(({ id }) => id === userId) as User;
                return of({ ...taskData, user }).pipe(
                    delay(300),
                    map((data) => TaskActions.saveTaskSuccess({ data })),
                    catchError((error) => of(TaskActions.saveTaskFailure({ error }))),
                );
            }),
        );
    });

    cleanCurrentTask$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(TaskActions.closeTaskModal),
            delay(CLEAN_CURRENT_TASK_DELAY),
            map(() => TaskActions.cleanCurrentTask()),
        );
    });

    constructor(private actions$: Actions) {}
}
