import { createFeature, createReducer, on } from "@ngrx/store";
import { TaskActions } from "./task.actions";
import { TASK_FEATURE_KEY } from "../../../core/constants/store-feature-keys";
import { Task } from "../../authorization/models/task";
import { User } from "../../authorization/models/user";
import { SortOption } from "../../../core/constants/sort-option";
import { TaskFormData } from "../../authorization/models/task-form-data";
import { sortOptionFunctionMap } from "../utils/task-sort-fns";

export interface TaskState {
    tasks: Task[];
    users: User[];
    usersLoading: boolean;
    tasksLoading: boolean;
    taskSaving: boolean;
    sortOption: SortOption;
    taskModalOpen: boolean;
    currentTask: TaskFormData | null;
}

export const initialState: TaskState = {
    tasks: [],
    users: [],
    tasksLoading: false,
    usersLoading: false,
    taskSaving: false,
    sortOption: SortOption.DATE_ASC,
    taskModalOpen: false,
    currentTask: null,
};

export const reducer = createReducer(
    initialState,
    on(TaskActions.loadTasks, (state): TaskState => ({ ...state, tasksLoading: true })),
    on(TaskActions.loadTasksSuccess, (state, { data }): TaskState => {
        const sorterFn = sortOptionFunctionMap[state.sortOption];
        return { ...state, tasks: sorterFn([...data]) };
    }),
    on(
        TaskActions.loadTasksFailure,
        TaskActions.loadTasksSuccess,
        (state): TaskState => ({
            ...state,
            tasksLoading: false,
        }),
    ),
    on(TaskActions.loadUsers, (state): TaskState => ({ ...state, usersLoading: true })),
    on(TaskActions.loadUsersSuccess, (state, { data }): TaskState => ({ ...state, users: data })),
    on(
        TaskActions.loadUsersFailure,
        TaskActions.loadUsersSuccess,
        (state): TaskState => ({
            ...state,
            usersLoading: false,
        }),
    ),
    on(TaskActions.saveTask, (state): TaskState => ({ ...state, taskSaving: true })),
    on(TaskActions.saveTaskSuccess, (state, { data }): TaskState => {
        const sorterFn = sortOptionFunctionMap[state.sortOption];
        return { ...state, tasks: sorterFn([...state.tasks, data]), taskModalOpen: false };
    }),
    on(
        TaskActions.saveTaskSuccess,
        TaskActions.saveTaskFailure,
        (state): TaskState => ({ ...state, taskSaving: false }),
    ),
    on(TaskActions.setTaskSortOption, (state, { data }): TaskState => {
        const sorterFn = sortOptionFunctionMap[data];
        return { ...state, sortOption: data, tasks: sorterFn([...state.tasks]) };
    }),
    on(
        TaskActions.openTaskModal,
        (state, { data }): TaskState => ({
            ...state,
            currentTask: data ?? state.currentTask,
            taskModalOpen: true,
        }),
    ),
    on(TaskActions.closeTaskModal, (state): TaskState => ({ ...state, taskModalOpen: false })),
    on(TaskActions.cleanCurrentTask, (state): TaskState => ({ ...state, currentTask: null })),
    on(
        TaskActions.deleteTask,
        (state, { data }): TaskState => ({
            ...state,
            tasks: state.tasks.filter((_, i) => i !== data),
        }),
    ),
);

export const taskFeature = createFeature({
    name: TASK_FEATURE_KEY,
    reducer,
});
