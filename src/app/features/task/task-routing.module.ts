import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppRoutes } from "../../core/constants/app-routes";
import { TasksPageComponent } from "./pages/tasks-page/tasks-page.component";

const routes: Routes = [{ path: AppRoutes.Index, component: TasksPageComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TaskRoutingModule {}
