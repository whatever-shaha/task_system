import { User } from "../../app/features/authorization/models/user";

export const MOCK_USERS: User[] = [
    {
        id: 1,
        displayName: "Alice Johnson",
        email: "alice@example.com",
        avatar: "avatar1",
    },
    {
        id: 2,
        displayName: "Bob Smith",
        email: "bob@example.com",
        avatar: "avatar2",
    },
    {
        id: 3,
        displayName: "Charlie Brown",
        email: "charlie@example.com",
        avatar: "avatar3",
    },
    {
        id: 4,
        displayName: "David Lee",
        email: "david@example.com",
        avatar: "avatar4",
    },
    {
        id: 5,
        displayName: "Eva Martinez",
        email: "eva@example.com",
        avatar: "avatar5",
    },
    {
        id: 6,
        displayName: "Frank Adams",
        email: "frank@example.com",
        avatar: "avatar6",
    },
    {
        id: 7,
        displayName: "Grace Wilson",
        email: "grace@example.com",
        avatar: "avatar7",
    },
    {
        id: 8,
        displayName: "Henry Taylor",
        email: "henry@example.com",
        avatar: "avatar8",
    },
    {
        id: 9,
        displayName: "Isabella Clark",
        email: "isabella@example.com",
        avatar: "avatar9",
    },
];

export const MOCK_APP_USERS = [
    {
        id: 1,
        displayName: "Alice Johnson",
        email: "alice@example.com",
        avatar: "avatar1",
        password: "alice@example.com$pass",
    },
    {
        id: 2,
        displayName: "Bob Smith",
        email: "bob@example.com",
        avatar: "avatar2",
        password: "bob@example.com$pass",
    },
    {
        id: 3,
        displayName: "Charlie Brown",
        email: "charlie@example.com",
        avatar: "avatar3",
        password: "charlie@example.com$pass",
    },
    {
        id: 4,
        displayName: "David Lee",
        email: "david@example.com",
        avatar: "avatar4",
        password: "david@example.com$pass",
    },
    {
        id: 5,
        displayName: "Eva Martinez",
        email: "eva@example.com",
        avatar: "avatar5",
        password: "eva@example.com$pass",
    },
    {
        id: 6,
        displayName: "Frank Adams",
        email: "frank@example.com",
        avatar: "avatar6",
        password: "frank@example.com$pass",
    },
    {
        id: 7,
        displayName: "Grace Wilson",
        email: "grace@example.com",
        avatar: "avatar7",
        password: "grace@example.com$pass",
    },
    {
        id: 8,
        displayName: "Henry Taylor",
        email: "henry@example.com",
        avatar: "avatar8",
        password: "henry@example.com$pass",
    },
    {
        id: 9,
        displayName: "Isabella Clark",
        email: "isabella@example.com",
        avatar: "avatar9",
        password: "isabella@example.com$pass",
    },
];
